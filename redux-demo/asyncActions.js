const { createStore, applyMiddleware } = require('redux');
const thunkMiddleware = require('redux-thunk').default;
const axios = require('axios');

const initialState = {
  loading: false,
  users: [],
  error: '',
}

const FETCH_USERS_RERQUESTED = 'FETCH_USERS_RERQUESTED';
const FETCH_USERS_SUCCEEDED = 'FETCH_USERS_SUCCEEDED';
const FETCH_USERS_FAILED = 'FETCH_USERS_FAILED';

const fetchUserRequest = () => {
  return {
    type: FETCH_USERS_RERQUESTED,
  }
}

const fetchUsersSuccess = (user) => {
  return {
    type: FETCH_USERS_SUCCEEDED,
    payload: user,
  }
}

const fetchUserFailure = (error) => {
  return {
    type: FETCH_USERS_FAILED,
    payload: error,
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_RERQUESTED:
      return {
        ...state,
        loading: true,
      }
    case FETCH_USERS_SUCCEEDED:
      return {
        loading: false,
        users: action.payload,
        error: ''
      }
    case FETCH_USERS_FAILED:
      return {
        loading: false,
        users: [],
        error: action.payload,
      }
  }
}

const fetchUsers = () => {
  return function(dispatch) {
    dispatch(fetchUserRequest())
    axios.get('https://jsonplaceholder.typicode.com/users/as')
    .then((response) => {
      const users = response.data.map((user) => user.id);
      dispatch(fetchUsersSuccess(users));
    })
    .catch((error) => {
      dispatch(fetchUserFailure(error.message))
    })
  }
}

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

store.subscribe(() => { console.log('init ', store.getState()) });
store.dispatch(fetchUsers());
