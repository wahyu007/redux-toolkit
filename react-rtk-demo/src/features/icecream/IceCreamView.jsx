import { useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import { ordered, restocked } from  "./iceCreamSlice";

export const IceCreamView = () => {
  const numOfIceCreams = useSelector((state) => state.icecream.numOfIceCreams);
  const [value, setValue] = useState(0);
  const dispatch = useDispatch();
  return (
    <div>
      <h2>Number of ice creams - {numOfIceCreams}</h2>
      <button onClick={() => dispatch(ordered())}>Order cream</button>
      <input type="number" value={value} onChange={((e) => setValue(e.target.value))} />
      <button onClick={() => dispatch(restocked(value))}>Restock creams</button>
    </div>
  )
}
