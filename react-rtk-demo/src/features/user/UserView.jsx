import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { fetchUSers } from './userSlice';

export const UserView = () => {
  const user = useSelector((state) => state.user)
  const disptach = useDispatch();
  
  useEffect(() => {
    disptach(fetchUSers())
  }, [])

  console.log('user ', user);

  return (
    <div>
      <h2>List of Users</h2>
      { user.loading && <div>Loading</div> }
      { !user.loading && user.error ? <div>Error: {user.error}</div> : null }
      { !user.loading && user.users.length ? (
        <ul>
          {user.users.map((u) => (
            <li key={u.id}>{u.name}</li>
          ))}
        </ul>
      ) : null}
    </div>
  )
}
