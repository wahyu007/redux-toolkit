import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
  loading: false,
  users: [],
  error: '',
}


// Generate pending, fulfiled and rejected action types
export const fetchUSers = createAsyncThunk('user/fetchUsers', () => {
  return axios.get('https://jsonplaceholder.typicode.com/users/a')
  .then((response) => response.data)
})

const userSlice = createSlice({
  name: 'user',
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchUSers.pending, (state) => {
      state.loading = true;
    })
    builder.addCase(fetchUSers.fulfilled, (state, action) => {
      state.loading = false;
      state.users = action.payload;
      state.error = ''
    })
    builder.addCase(fetchUSers.rejected, (state, action) => {
      state.loading = false;
      state.users = [];
      state.error = action.error.message;
    })
  }
})

export default userSlice.reducer;
